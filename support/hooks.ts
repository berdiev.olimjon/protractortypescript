const { Before, After, AfterAll, Status } = require("cucumber");
import * as fs from "fs";
import { browser } from "protractor";
import { config } from "../config/config";

Before({timeout: 100 * 1000}, async () => {
    let url = await browser.getCurrentUrl();
    if(!url.includes('data')){
        await browser.restart();
        await browser.waitForAngularEnabled(false);
        browser.ignoreSynchronization = true;
        await browser.manage().window().maximize();
    } 
});

After(async function(scenario) {
    if (scenario.result.status === Status.FAILED) {
        // screenShot is a base-64 encoded PNG
         const screenShot = await browser.takeScreenshot();
         this.attach(screenShot, "image/png");
    }
});


